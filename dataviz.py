import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc
from bird_funcs import cache_data, \
    group_animal, \
    num_to_month, \
    prepare_data

import os
import plotly.graph_objects as go
import plotly.express as px
import datetime
import configparser
import numpy as np
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('[%(levelname)s] / %(name)s->%(message)s')
file_handler = logging.FileHandler('run.log')
file_handler.setFormatter(formatter)

stream_handler = logging.StreamHandler()
format_stream = logging.Formatter('[%(levelname)s]->%(message)s')
stream_handler.setFormatter(format_stream)

logger.addHandler(stream_handler)
logger.addHandler(file_handler)


config = configparser.ConfigParser()
config.read("config.ini")

df = prepare_data(config)  # create dataframe from pickled cache
animal_list = df["Nom"].unique().tolist()
location_list = df["Localisation"].unique().tolist()
dept_list = sorted(df["Dept"].dropna().unique().tolist(), reverse=True)
year_list = sorted(df["date"].dt.year.unique())

animal_list.sort()
location_list.sort()

app = dash.Dash()
app.layout = html.Div([html.H1(children="Statistique Dossier: {}".
                               format(os.path.basename(
                                   config["PATHS"]["photo folder"])),
                               style={'textAlign': 'center'}),

                       html.Label(["Selectionner une espèce:",
                                   dcc.Dropdown(
                                       value=[],
                                       options=[{'label': i, 'value': i}
                                                for i in animal_list],
                                       multi=True,
                                       id='dropdown1',
                                       style={'height': '50px',
                                              'width': '1000px'},
                                       placeholder="Sélectionner une espèce"
                                   )]),
                       html.Label(["Filtrer par Localisation:",
                                   dcc.Dropdown(
                                       value=[],
                                       options=[{'label': i, 'value': i}
                                                for i in location_list],
                                       multi=True,
                                       id='dropdown2',
                                       style={'height': '50px',
                                              'width': '1000px'},
                                       placeholder="Filter par localisation"
                                   )]),
                       html.Label(["Filtrer par Département:",
                                   dcc.Dropdown(
                                       value=[],
                                       options=[{'label': i, 'value': i}
                                                for i in dept_list],
                                       multi=True,
                                       id='dropdown3',
                                       style={'height': '50px',
                                              'width': '1000px'},
                                       placeholder="Filter par Département"
                                   )]),
                       html.Label(["Filtrer par année:",
                                   dcc.Dropdown(
                                       value=[],
                                       options=[{'label': i, 'value': i}
                                                for i in year_list],
                                       multi=True,
                                       id='dropdown4',
                                       style={'height': '50px',
                                              'width': '600px'},
                                       placeholder="Filter par année"
                                   )]),
                       html.Div([
                           dcc.Graph(id='our_graph1')],
    className='nine columns'),

    html.Div([
        dcc.Graph(id='our_graph2')
    ],

    className='nine columns'),


])


@app.callback(
    [Output('our_graph1', 'figure'), Output('our_graph2', 'figure')],
    [Input('dropdown1', 'value'),
     Input('dropdown2', 'value'),
     Input('dropdown3', 'value'),
     Input('dropdown4', 'value')]
)
def update_plots(cols, locs, depts, years):
    """updates the to plots based on dropdowns

    Parameters
    ----------
    cols : list of str
        input of dropdown1
    locs : list of str
        input of dropdown2
    depts : list of str
        input of dropdown3
    years : list of str
        input of dropdown4

    Returns
    -------
    tuple of figure (returned by px.bar())
        (fig1,fig2)
    """

    mask = np.full((len(df)), True)

    if len(cols) > 0:
        mask *= df["Nom"].isin(cols).values

    if len(depts) > 0:
        mask *= df["Dept"].isin(depts).values

    if len(locs) > 0:
        mask *= df["Localisation"].isin(locs).values

    if len(years) > 0:
        mask *= df.date.dt.year.isin(years).values

    data = group_animal(df.loc[mask, :], group="year")
    fig1 = px.bar(data, x="date", y="Count",
                  color='Nom')

    fig1.update_layout(yaxis={'title': 'Nb cumulé'},
                       xaxis={"title": "Année", "range": [
                           df.date.dt.year.min()-1, df.date.dt.year.max()+1]},
                       height=300)

    data = group_animal(df.loc[mask, :], group="Localisation")
    fig2 = px.bar(data, x="Localisation", y="Count", color='Nom')

    fig2.update_layout(yaxis={'title': 'Nb cumulé'},
                       xaxis={"title": "Localisation"},
                       height=300, showlegend=False)

    return fig1, fig2


if __name__ == '__main__':
    logger.info("----- Run {} ----- ".format(datetime.datetime.today()))
    app.run_server(debug=False)
